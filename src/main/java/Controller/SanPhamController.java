package Controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import Entity.DanhMucSanPham;
import Entity.SanPham;
import Services.DanhMucServices;
import Services.SanPhamService;

@RequestMapping("/sanpham")
@Controller
public class SanPhamController {
	@Autowired
	DanhMucServices danhMucService;
	@Autowired
	SanPhamService spService;
	
			@GetMapping("/{madanhmucsanpham}")
			public String Default(ModelMap mm,@PathVariable int madanhmucsanpham ) {
				//list danh muc
						List<DanhMucSanPham> danhmucs=danhMucService.LayDanhMuc();
						mm.addAttribute("danhMuc", danhmucs);
						
				//lay ra san pham theo ma
						List<SanPham> sP=spService.LaydanhsachsanphamTheoMa(madanhmucsanpham);
						mm.addAttribute("sanPhamTheoDanhMuc",sP);
						//lay ra ten danh muc
						DanhMucSanPham dm=danhMucService.Lay1DanhMucTheoMa(madanhmucsanpham);
						mm.addAttribute("sigleDanhMuc",dm);
						
				return "sanpham";
			}
}
