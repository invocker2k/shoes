package Controller;

import javax.servlet.http.HttpSession;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import Entity.ChucVu;
import Services.ChucVuSevice;
import Services.NhanVienService;

@Controller
@RequestMapping("/login")
@SessionAttributes("User")
public class LoginController {
	@Autowired
	ChucVuSevice chucvu;
	@Autowired
	NhanVienService nhanvienser;
	@Autowired
	SessionFactory SessionFactory;
	@GetMapping
	public String login(ModelMap mm,HttpSession httpsession) {
		if(httpsession.getAttribute("User")!=null) {
			String name=(String) httpsession.getAttribute("User");
		String	headerUser=name.substring(0, 1);
			mm.addAttribute("headerUser",headerUser);
		}
		return "login";
	}
	@PostMapping(params = "signin")
	public String DangNhap(@RequestParam String Username,@RequestParam String password,ModelMap modemap,HttpSession httpss) {
		
		if(	nhanvienser.KiemTraDangNhap(Username, password)) {
			modemap.addAttribute("User",Username);
			String	headerUser=Username.substring(0, 1);
			modemap.addAttribute("headerUser",headerUser);
			return "index";
	}
	modemap.addAttribute("kiemtradangnhap","Đăng nhập thất bại! Hãy thử lại.");
	return  "login";
		
}
	@PostMapping(params = "signup")
	public String DangKi(@RequestParam String tennhanvien,@RequestParam String hoten,@RequestParam String diachi,@RequestParam Boolean gioitinh,@RequestParam String cmnd,@RequestParam ChucVu machucvu,@RequestParam String email,@RequestParam String matkhau) {
		ChucVu cv=new ChucVu("giamdoc");
		nhanvienser.ThemNhanVien(tennhanvien, hoten, diachi, gioitinh, cmnd,cv, email, matkhau);
		return "login";
	}
	
}
