package Controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import Entity.GioHang;
import Services.SuggestionsService;

@Controller
@RequestMapping("api/")
@SessionAttributes("giohang")
public class APIController {
	
	@Autowired
	SuggestionsService suggsevice;
	@GetMapping("gopy")
	@ResponseBody
	public String GopY(@RequestParam String name,@RequestParam String context) {
		suggsevice.SetSuggestions(name, context);
		return name;
	}
	@SuppressWarnings("unchecked")
	@GetMapping("Themgiohang")
	@ResponseBody
	public void ThemGioHang(HttpSession sesion,@RequestParam String tenSP,@RequestParam String giaTien,@RequestParam int soLuong,@RequestParam int masp,@RequestParam int masize,@RequestParam String tenmau,@RequestParam String tensize,@RequestParam int mamau,@RequestParam int maChiTietSP) {
		
		if(null==sesion.getAttribute("giohang")) {
		
			List<GioHang> gioHangs=new ArrayList<GioHang>();
			GioHang gioHang=new GioHang();
			gioHang.setGiaTien(giaTien);
			gioHang.setTenSP(tenSP);
			gioHang.setMaChiTietSP(maChiTietSP);
			gioHang.setMamau(mamau);
			gioHang.setMasize(masize);
			gioHang.setSoLuong(soLuong);
			gioHang.setSoLuong(1);
			gioHang.setTenmau(tenmau);
			gioHang.setTensize(tensize);
			gioHang.setMasp(masp);
			gioHangs.add(gioHang);
			sesion.setAttribute("giohang", gioHangs);
			List<GioHang> giohangx=(List<GioHang>) sesion.getAttribute("giohang");
		System.out.println(giohangx.size());
		}
		else {
			List<GioHang> sessionOld=(List<GioHang>) sesion.getAttribute("giohang");

			int viTri=KiemTraSanPhamCoTrongGioHang(sesion, tenSP, giaTien,soLuong,masp,masize,tenmau,tensize,mamau);
			if(viTri==-1) {
				System.out.println("s p ko co trong gio hang");
				GioHang gioHang=new GioHang();
				gioHang.setGiaTien(giaTien);
				gioHang.setTenSP(tenSP);
				gioHang.setMamau(mamau);
				gioHang.setMaChiTietSP(maChiTietSP);
				gioHang.setMasize(masize);
				gioHang.setSoLuong(soLuong);
				gioHang.setSoLuong(1);
				gioHang.setTenmau(tenmau);
				gioHang.setTensize(tensize);
				gioHang.setMasp(masp);
				sessionOld.add(gioHang);
				System.out.println(sessionOld.size()+" - "+sesion.getAttribute("giohang"));
			}
			else {
				int soLuongMoi=sessionOld.get(viTri).getSoLuong()+1;
				sessionOld.get(viTri).setSoLuong(soLuongMoi);
			}
			List<GioHang> test=(List<GioHang>) sesion.getAttribute("giohang");
			for (GioHang gioHang : test) {
				System.out.println(gioHang.getTenSP()+" - "+gioHang.getSoLuong());
			}
			
		}
	}
	// kiem tra
	private int KiemTraSanPhamCoTrongGioHang(HttpSession sesion, String tenSP, String giaTien, int soLuong,int maSP, int maSize, String tenmau, String tensize,int mamau) {
		List<GioHang> sessionOld=(List<GioHang>) sesion.getAttribute("giohang");
		for (int i = 0; i < sessionOld.size(); i++) {
			System.out.println("mamau: "+sessionOld.get(i).getMamau()+"-"+mamau+"\n masize"+sessionOld.get(i).getMasize()+" - "+maSize+"\n ma sp"+sessionOld.get(i).getMasp()+" - "+maSP+"\n");
			if( sessionOld.get(i).getMamau()==mamau&&sessionOld.get(i).getMasize()==maSize&&sessionOld.get(i).getMasp()==maSP) {
				return i;
			}
		}
		return -1;
	}
	@GetMapping("LaySoLuongGioHang")
	@ResponseBody
	public String LaySoLuongGioHang(HttpSession httpsession) {
		if(httpsession.getAttribute("giohang")!=null) {
			List<GioHang> listGioHang =(List<GioHang>)httpsession.getAttribute("giohang");
			System.out.println(listGioHang.size()+"here is controller");
			return listGioHang.size()+"";
			
		}
		return "";
	}
	@GetMapping("capnhatgiohang")
	@ResponseBody
	public void CatNhatGioHang(HttpSession httpsession,@RequestParam int soLuong,@RequestParam int maSP,@RequestParam int mamau,@RequestParam int maSize) {
		if(httpsession.getAttribute("giohang")!=null) {
		
			List<GioHang>  sessionOld=(List<GioHang>) httpsession.getAttribute("giohang");
			
			for (int i = 0; i < sessionOld.size(); i++) {
				System.out.println("mamau: "+sessionOld.get(i).getMamau()+"-"+mamau+"\n"+sessionOld.get(i).getMasize()+" - "+maSize+"\n"+sessionOld.get(i).getMasp()+" - "+maSP+"\n"+sessionOld.get(i).getSoLuong()+" - "+soLuong);
				if( sessionOld.get(i).getMamau()==mamau&&sessionOld.get(i).getMasize()==maSize&&sessionOld.get(i).getMasp()==maSP) {
					sessionOld.get(i).setSoLuong(soLuong);
					if(sessionOld.get(i).getSoLuong()!=soLuong) {break;}
					
				}
			}
		}
		
	}
	@GetMapping("xoasp")
	private String DeleteSP(HttpSession sesion,@RequestParam String tenSP,@RequestParam int soLuong,@RequestParam String tenmau,@RequestParam String tensize) {
		List<GioHang> sessionOld=(List<GioHang>) sesion.getAttribute("giohang");
		System.out.println("so luong truoc xoa:"+sessionOld.size()+"\n");
		for (int i = 0; i < sessionOld.size(); i++) {
			System.out.println("sten"+sessionOld.get(i).getTenSP()+"-VS-"+tenSP+"\n");
			
			if(sessionOld.get(i).getTenSP().equals(tenSP) &&sessionOld.get(i).getSoLuong()==soLuong&&sessionOld.get(i).getTenmau().equals(tenmau)&&sessionOld.get(i).getTensize().equals(tensize)) {
					sessionOld.remove(i);
					System.out.println("so luong sau khi xoa:"+sessionOld.size()+"\n");

			}
		}
		return "giohang";
	}
}
