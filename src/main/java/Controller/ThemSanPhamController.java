package Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import Entity.SanPham;
import Services.SanPhamService;

@Controller
@RequestMapping("themsanpham")
public class ThemSanPhamController {
	@Autowired
	SanPhamService sPSevice;
	
	@GetMapping
	public String Defaut(ModelMap mm) {
		List<SanPham> listSanPham=sPSevice.Laydanhsachsanpham(0, 3);
		List<SanPham> allSanPham=sPSevice.LayAllSanPham();
		
		double tongSoPage=Math.ceil((double) allSanPham.size()/3);
		mm.addAttribute("listSP",listSanPham);
		mm.addAttribute("allSP",allSanPham);
		mm.addAttribute("tongSoPage",tongSoPage);
		return "themsanpham";
	}
}
