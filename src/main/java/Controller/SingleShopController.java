package Controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import Entity.DanhMucSanPham;
import Entity.GioHang;
import Entity.SanPham;
import Services.DanhMucServices;
import Services.SanPhamService;

@Controller
@RequestMapping("/singleshop")
@SessionAttributes("giohang")
public class SingleShopController {
	
	@Autowired
	DanhMucServices danhMucService;
	@Autowired
	SanPhamService spsv;
	
		@GetMapping("/{masanpham}")
		public String SingleShop(ModelMap mm,HttpSession httpsession,@PathVariable int masanpham) {
			if(httpsession.getAttribute("User")!=null) {
				String name=(String) httpsession.getAttribute("User");
			String	headerUser=name.substring(0, 1);
				mm.addAttribute("headerUser",headerUser);
			}
			SanPham sPham=spsv.LaydanhsachChiTietsanpham(masanpham);
			//xy ly list gio hang
			if(httpsession.getAttribute("giohang")!=null) {
				List<GioHang> listGioHang =(List<GioHang>)httpsession.getAttribute("giohang");
				System.out.println(listGioHang.size()+"here is controller");
				mm.addAttribute("soLuongSanPham",listGioHang.size());
				
			}
			mm.addAttribute("sp",sPham);
			//lay list san pham
			List<SanPham> listSP= spsv.Laydanhsachsanpham(0,4);
			mm.addAttribute("listSP",listSP);
			
			//lay danhmuc
			List<DanhMucSanPham> danhMucsp=danhMucService.LayDanhMuc();
			mm.addAttribute("danhMuc",danhMucsp);
			return "singleshop";
		}
	
}
