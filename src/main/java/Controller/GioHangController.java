package Controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import Entity.ChiTietHoaDon;
import Entity.ChiTietHoaDonId;
import Entity.GioHang;
import Entity.HoaDon;
import Services.ChiTietHoaDonService;
import Services.HoaDonService;

@Controller
@RequestMapping("/giohang")
public class GioHangController {
	@Autowired
	HoaDonService hDService;
	@Autowired
	ChiTietHoaDonService cthdService;
	@GetMapping
	public String TabGioHang(HttpSession session,ModelMap mm) {
		if(null != session.getAttribute("giohang")) {
			
			List<GioHang> gioHangs=(List<GioHang>) session.getAttribute("giohang");
			mm.addAttribute("soluongsanphamgiohang",gioHangs.size());
			mm.addAttribute("giohangs",gioHangs);
			
		}
		return "giohang";
	}
	@PostMapping
	public String ThemhoaDon(HttpSession httpSession,
								@RequestParam  String tenkhachhang,
								@RequestParam String sdt,
								@RequestParam String noigiaohang,
								@RequestParam String hinhthucgiaohang,
								@RequestParam String ghichu) {
							//lay ra gio hang truoc
			if(null != httpSession.getAttribute("giohang")) {
			List<GioHang> gioHangs=(List<GioHang>) httpSession.getAttribute("giohang");
						//save hoa don
						HoaDon hoaDon=new HoaDon();
						hoaDon.setGhichu(ghichu);
						hoaDon.setHinhthucgiaohang(hinhthucgiaohang);
						hoaDon.setNoigiaohang(noigiaohang);
						hoaDon.setTenkhachhang(tenkhachhang);
						hoaDon.setSdt(sdt);
						hDService.AddBill(hoaDon);
						if(hDService.AddBill(hoaDon)==1) {
							Set<ChiTietHoaDon> cthds=new HashSet<ChiTietHoaDon>();
							for (GioHang gioHang : gioHangs) {
								//cthd ID
								ChiTietHoaDonId cthdID=new ChiTietHoaDonId();
								cthdID.setMachitietsanpham(gioHang.getMaChiTietSP());
								cthdID.setMahoadon(hoaDon.getMahoadon());
								//CHi TIET HOA DON
								ChiTietHoaDon cthd=new ChiTietHoaDon();
								cthd.setChiTietHoaDonId(cthdID);
								cthd.setGiatien(gioHang.getGiaTien());
								cthd.setSoluong(gioHang.getSoLuong());
								cthdService.ThemChiTietHoaDon(cthd);
							}
							System.out.println("them hoa don thanh cong");
						}
						else {
							System.out.println("them hoa don khong thanh cong");
						}				
						
			}
			
						return "giohang";
					}
}
