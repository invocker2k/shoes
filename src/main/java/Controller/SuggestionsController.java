package Controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/gopy")
public class SuggestionsController {
	@GetMapping
	public String GopY(ModelMap mm,HttpSession httpsession) {
		if(httpsession.getAttribute("User")!=null) {
			String name=(String) httpsession.getAttribute("User");
		String	headerUser=name.substring(0, 1);
			mm.addAttribute("headerUser",headerUser);
		}
		return "suggestions";
	}
}
