package Controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;

import Entity.DanhMucSanPham;
import Entity.SanPham;
import Services.DanhMucServices;
import Services.SanPhamService;

@Controller
@RequestMapping({"/","trangchu"})

public class TrangChuController {
	@Autowired
	DanhMucServices danhMucService;
	@Autowired
	SanPhamService spService;
	
	@GetMapping
	@Transactional
	public String index(ModelMap mm,HttpSession httpsession) {
		//session
		if(httpsession.getAttribute("User")!=null) {
			String name=(String) httpsession.getAttribute("User");
		String	headerUser=name.substring(0, 1);
			mm.addAttribute("headerUser",headerUser);
		}
		
		//done session
		
		//lay list san pham
		List<SanPham> listSP= spService.Laydanhsachsanpham(0,20);
		mm.addAttribute("listSP",listSP);
		
		//list danh muc
		List<DanhMucSanPham> danhmucs=danhMucService.LayDanhMuc();
		mm.addAttribute("danhMuc", danhmucs);
		return ("index");
	}
/*	@GetMapping("trangchu")
	public String trangchu(ModelMap mm,HttpSession httpsession) {
		if(httpsession.getAttribute("User")!=null) {
			String name=(String) httpsession.getAttribute("User");
		String	headerUser=name.substring(0, 1);
			mm.addAttribute("headerUser",headerUser);
		}
		List<SanPham> listSP= spService.Laydanhsachsanpham(0);
		mm.addAttribute("listSP",listSP);
		return ("index");
	}*/
}


