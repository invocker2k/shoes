package ModelAPI;

import java.util.List;

import Entity.DanhMucSanPham;

public interface DanhMucAPI {
		List<DanhMucSanPham>  LayDanhMuc();
		DanhMucSanPham Lay1DanhMucTheoMa(int maDanhMuc);
}
