package ModelAPI;

import java.util.List;

import Entity.SanPham;

public interface SanPhamAPI {
	List<SanPham> Laydanhsachsanpham(int spBatDau,int spEnd);
	SanPham LaydanhsachChiTietsanpham(int maSanPham);
	List<SanPham> LayAllSanPham();
	List<SanPham> LaydanhsachsanphamTheoMa(int maDSSP);
}
