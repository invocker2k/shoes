package ModelAPI;

import Entity.ChucVu;
import Entity.NhanVien;

public interface NhanVienAPI {

	void ThemNhanVien(String tendangnhap,String hoten,String diachi, Boolean gioitinh,String cmnd,ChucVu chucvu,String email,String matkhau);
	NhanVien LoginNhanVien(String name,String pass);
	boolean KiemTraDangNhap(String name,String pass);
}
