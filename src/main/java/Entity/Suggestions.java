package Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "suggestions")
public class Suggestions {
	@Id
	@GeneratedValue(strategy  = GenerationType.IDENTITY)
	private int idsuggestions;
	private String tenNguoiGopY;
	private String noiDung;
	public int getIdsuggestions() {
		return idsuggestions;
	}
	public void setIdsuggestions(int idsuggestions) {
		this.idsuggestions = idsuggestions;
	}
	public String getTenNguoiGopY() {
		return tenNguoiGopY;
	}
	public void setTenNguoiGopY(String tenNguoiGopY) {
		this.tenNguoiGopY = tenNguoiGopY;
	}
	public String getNoiDung() {
		return noiDung;
	}
	public void setNoiDung(String noiDung) {
		this.noiDung = noiDung;
	}
	
}
