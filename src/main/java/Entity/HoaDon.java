package Entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity(name="hoadon")
public class HoaDon {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int mahoadon;
	private String tenkhachhang;
	private String sdt;
	private String noigiaohang;
	private boolean tinhtrang;
	private String ngaylap;
	private String hinhthucgiaohang;
	private String ghichu;
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "mahoadon")
	Set<ChiTietHoaDon> danhSachchitiethoaDon;
	
	public String getHinhthucgiaohang() {
		return hinhthucgiaohang;
	}

	public void setHinhthucgiaohang(String hinhthucgiaohang) {
		this.hinhthucgiaohang = hinhthucgiaohang;
	}

	public String getGhichu() {
		return ghichu;
	}

	public void setGhichu(String ghichu) {
		this.ghichu = ghichu;
	}

	public int getMahoadon() {
		return mahoadon;
	}

	public void setMahoadon(int mahoadon) {
		this.mahoadon = mahoadon;
	}

	public String getTenkhachhang() {
		return tenkhachhang;
	}

	public void setTenkhachhang(String tenkhachhang) {
		this.tenkhachhang = tenkhachhang;
	}

	public String getSdt() {
		return sdt;
	}

	public void setSdt(String sdt) {
		this.sdt = sdt;
	}

	public String getNoigiaohang() {
		return noigiaohang;
	}

	public void setNoigiaohang(String noigiaohang) {
		this.noigiaohang = noigiaohang;
	}

	public boolean isTinhtrang() {
		return tinhtrang;
	}

	public void setTinhtrang(boolean tinhtrang) {
		this.tinhtrang = tinhtrang;
	}

	public String getNgaylap() {
		return ngaylap;
	}

	public void setNgaylap(String ngaylap) {
		this.ngaylap = ngaylap;
	}

	public Set<ChiTietHoaDon> getDanhSachchitiethoaDon() {
		return danhSachchitiethoaDon;
	}

	public void setDanhSachchitiethoaDon(Set<ChiTietHoaDon> danhSachchitiethoaDon) {
		this.danhSachchitiethoaDon = danhSachchitiethoaDon;
	}

	
}
