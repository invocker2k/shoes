package DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import Entity.ChiTietSanPham;
import Entity.SanPham;
import ModelAPI.SanPhamAPI;

@Repository
@Scope(proxyMode=ScopedProxyMode.TARGET_CLASS)
public class SanPhamDao implements SanPhamAPI{
		
	@Autowired
	SessionFactory sessionfactory;
	@Transactional
	public List<SanPham> LaydanhsachsanphamTheoMa(int madanhmuc) {
		Session session=sessionfactory.getCurrentSession();
		List<SanPham> listSanPham=(List<SanPham>)session.createQuery("from sanpham where madanhmuc ="+madanhmuc).getResultList();
		return listSanPham;
	}
	@Transactional
	public List<SanPham> Laydanhsachsanpham(int spBatDau,int spEnd) {
		// TODO Auto-generated method stub
		Session session=sessionfactory.getCurrentSession();
		List<SanPham> listSanPham=(List<SanPham>)session.createQuery("from sanpham").setFirstResult(spBatDau).setMaxResults(spEnd).getResultList();
		return listSanPham;
	}
	@Transactional
	public SanPham LaydanhsachChiTietsanpham(int maDSSanPham) {
	//	System.out.print("vo toi day r");
		// TODO Auto-generated method stub
		Session session=sessionfactory.getCurrentSession();
		SanPham SPhams = (SanPham) session.createQuery("from  sanpham sp where sp.masanpham="+maDSSanPham).getSingleResult();
		
		return SPhams;
	}
	
	@Transactional
	public List<SanPham> LayAllSanPham() {
		// TODO Auto-generated method stub
		Session session=sessionfactory.getCurrentSession();
		List<SanPham> listSanPham=(List<SanPham>)session.createQuery("from sanpham").getResultList();
		return listSanPham;
	}

	
}
