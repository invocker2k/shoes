package DAO;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import Entity.ChucVu;
import Entity.NhanVien;
import ModelAPI.NhanVienAPI;

@Repository
@Scope(proxyMode=ScopedProxyMode.TARGET_CLASS)
public class NhanVienDAO implements NhanVienAPI{
	@Autowired 
	 SessionFactory tx;
@Transactional
public void ThemNhanVien(String tendangnhap, String hoten, String diachi, Boolean gioitinh, String cmnd,
			ChucVu chucvu, String email, String matkhau) {
		// TODO Auto-generated method stub
		Session session=tx.getCurrentSession();
		NhanVien nv=new NhanVien();
		nv.setChucvu(chucvu);
		nv.setEmail(email);
		nv.setDiachi(diachi);
		nv.setTendangnhap(tendangnhap);
		nv.setMatkhau(matkhau);
		nv.setHoten(hoten);
		nv.setCmnd(cmnd);
		nv.setGioitinh(gioitinh);
		session.save(nv);
		
	}
@Transactional
public NhanVien LoginNhanVien(String name, String pass) {
	// TODO Auto-generated method stub
	 Session ss=tx.getCurrentSession();
	
	try {
		 NhanVien nv= (NhanVien) ss.createQuery("from nhanvien where tendangnhap='"+name+"' and matkhau='"+pass+"'").getSingleResult();
	return nv; 
	}catch (Exception e) {
		// TODO: handle exception
		return null;
	}
	
}
@Transactional

public boolean KiemTraDangNhap(String name, String pass) {
	
	 Session ss=tx.getCurrentSession();
	try {
		 NhanVien nv= (NhanVien) ss.createQuery("from nhanvien where tendangnhap='"+ name +"' and matkhau='"+ pass +"'").getSingleResult();
		 if(nv!=null) {
			 
			 return true; 
			 
		 }
			return false;

	}catch (Exception e) {
		// TODO: handle exception
		return false;
		}
	
	}
}
