package DAO;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;

import Entity.ChiTietHoaDon;
import Entity.HoaDon;
import ModelAPI.HoaDonAPI;

@Repository
@Scope(proxyMode=ScopedProxyMode.TARGET_CLASS)
public class HoaDonDAO implements HoaDonAPI {
	@Autowired
	SessionFactory ssF;
		
	@Transactional
	public int AddBill(HoaDon hoaDon) {
		// TODO Auto-generated method stub
		Session ss=ssF.getCurrentSession();
		try {
			 ss.save(hoaDon);
				return 1;
		} catch (Exception e) {
			// TODO: handle exception
		
		}
		return 0;
	}
	
	
	
}
