package DAO;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import Entity.ChucVu;
import Services.ChucVuSeviceImp;

@Repository
@Scope(proxyMode=ScopedProxyMode.TARGET_CLASS)
public class ChucVuDAO {
	@Autowired
	SessionFactory sessionFactory;
	@Transactional
	public void AddChucVu(String name) {
		Session session=sessionFactory.getCurrentSession();
		ChucVu cv=new ChucVu(name);
		session.save(cv);
	}
	@Transactional
	public String ResulChucVu(int machucvu) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		ChucVu cv=session.get(ChucVu.class, machucvu);
		System.out.print("ten cv la :......"+cv.getTenchucvu()+"");
		return cv.getTenchucvu();
	}
}
