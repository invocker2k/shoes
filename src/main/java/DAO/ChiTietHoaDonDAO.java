package DAO;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;

import Entity.ChiTietHoaDon;
import ModelAPI.ChiTietHoaDonAPI;

@Repository
@Scope(proxyMode=ScopedProxyMode.TARGET_CLASS)

public class ChiTietHoaDonDAO implements ChiTietHoaDonAPI{
	@Autowired
	SessionFactory ssF;
	
	@Transactional
	public boolean ThemChiTietHoaDon(ChiTietHoaDon cthd) {
		// TODO Auto-generated method stub
		Session ss=ssF.getCurrentSession();
		try {
			ss.save(cthd);
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
		return true;
	}
		
}
