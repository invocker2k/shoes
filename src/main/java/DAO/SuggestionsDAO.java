package DAO;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import Entity.Suggestions;
import ModelAPI.SuggestionsAPI;
@Repository
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SuggestionsDAO implements SuggestionsAPI{
	@Autowired
	private SessionFactory sf;
	@Transactional
	public void SetSuggestions(String name, String context) {
		// TODO Auto-generated method stub
		Session session=sf.getCurrentSession();
		Suggestions gopy=new Suggestions();
		gopy.setNoiDung(context);
		gopy.setTenNguoiGopY(name);
		session.save(gopy);
	}
	
}
