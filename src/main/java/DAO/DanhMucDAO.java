package DAO;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;

import Entity.DanhMucSanPham;
import Entity.SanPham;
import ModelAPI.DanhMucAPI;
import Services.DanhMucServices;

@Repository
@Scope(proxyMode=ScopedProxyMode.TARGET_CLASS)
public class DanhMucDAO implements DanhMucAPI{
	
	@Autowired
	org.hibernate.SessionFactory ssF;
	@Transactional
	public List<DanhMucSanPham> LayDanhMuc() {
		// TODO Auto-generated method stub
	org.hibernate.Session ssSession=ssF.getCurrentSession();
	String sql= "from danhmucsanpham";
	List<DanhMucSanPham> sanPhams= ssSession.createQuery(sql).getResultList();
		return sanPhams;
	}
	@Transactional
	public DanhMucSanPham Lay1DanhMucTheoMa(int maDanhMuc) {
		// TODO Auto-generated method stub
		org.hibernate.Session ssSession=ssF.getCurrentSession();
		String sql= "from danhmucsanpham where madanhmuc ="+maDanhMuc;
		DanhMucSanPham lay1DanhMuc =(DanhMucSanPham)ssSession.createQuery(sql).getSingleResult();
		return lay1DanhMuc;
	}

}
