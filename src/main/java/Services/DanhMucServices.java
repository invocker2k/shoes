package Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import DAO.DanhMucDAO;
import Entity.DanhMucSanPham;
import ModelAPI.DanhMucAPI;

@Service
public class DanhMucServices implements DanhMucAPI{
	@Autowired
	DanhMucDAO dmDAO;

	public List<DanhMucSanPham> LayDanhMuc() {
		// TODO Auto-generated method stub
		return dmDAO.LayDanhMuc();
	}

	public DanhMucSanPham Lay1DanhMucTheoMa(int maDanhMuc) {
		// TODO Auto-generated method stub
		return dmDAO.Lay1DanhMucTheoMa(maDanhMuc);
	}
}
