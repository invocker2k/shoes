package Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import DAO.ChucVuDAO;
import Entity.ChucVu;

@Service
public class ChucVuSevice  {
	@Autowired
	private ChucVuDAO chucvudao;

	public void AddChucVu(String name) {
		// TODO Auto-generated method stub
		chucvudao.AddChucVu( name);
	}

	public String ResulChucVu(int machucvu) {
		// TODO Auto-generated method stub
		
		return chucvudao.ResulChucVu(machucvu);
	}
}
