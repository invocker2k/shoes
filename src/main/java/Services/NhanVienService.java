package Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import DAO.NhanVienDAO;
import Entity.ChucVu;
import Entity.NhanVien;
import ModelAPI.NhanVienAPI;

@Service
public class NhanVienService implements NhanVienAPI{
	@Autowired
	NhanVienDAO NVDAO;
	public void ThemNhanVien(String tennhanvien,String hoten,String diachi, Boolean gioitinh,String cmnd,ChucVu machucvu,String email,String matkhau) {
		NVDAO.ThemNhanVien(tennhanvien, hoten, diachi, gioitinh, cmnd, machucvu, email, matkhau);
	}
	public NhanVien LoginNhanVien(String name, String pass) {
		// TODO Auto-generated method stub
		return NVDAO.LoginNhanVien(name, pass);
		
	}
	public boolean KiemTraLogin(String name, String pass) {
		if(NVDAO.LoginNhanVien(name, pass) !=null){
			return true;
		}else {
			return false;
		}
	}
	public boolean KiemTraDangNhap(String name, String pass) {
		// TODO Auto-generated method stub
		if(NVDAO.KiemTraDangNhap(name, pass)) return true;
		return false;
	}
	
}
