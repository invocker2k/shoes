package Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import DAO.ChiTietHoaDonDAO;
import Entity.ChiTietHoaDon;
import ModelAPI.ChiTietHoaDonAPI;

@Service
public class ChiTietHoaDonService implements ChiTietHoaDonAPI{
		@Autowired
		ChiTietHoaDonDAO cthdDAO;

		public boolean ThemChiTietHoaDon(ChiTietHoaDon cthd) {
			// TODO Auto-generated method stub
			return cthdDAO.ThemChiTietHoaDon(cthd);
		}
		
}
