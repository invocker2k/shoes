package Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import DAO.SanPhamDao;
import Entity.SanPham;
import ModelAPI.SanPhamAPI;

@Service
public class SanPhamService implements SanPhamAPI{
	@Autowired
	SanPhamDao sanPham;

	public List<SanPham> Laydanhsachsanpham(int spBatDau,int spEnd) {
		// TODO Auto-generated method stub
		
		return sanPham.Laydanhsachsanpham(spBatDau, spEnd);
	}

	public SanPham LaydanhsachChiTietsanpham(int maSanPham) {
		// TODO Auto-generated method stub
		return sanPham.LaydanhsachChiTietsanpham(maSanPham);
	}

	public List<SanPham> LayAllSanPham() {
		// TODO Auto-generated method stub
		return sanPham.LayAllSanPham();
	}

	public List<SanPham> LaydanhsachsanphamTheoMa(int maDSSP) {
		// TODO Auto-generated method stub
		return sanPham.LaydanhsachsanphamTheoMa(maDSSP);
	}
		
}
