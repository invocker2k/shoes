<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

</head>
<body>
        <!-- header -->
        <header class="header">
            <div class="container-fluid px-lg-5">
                <!-- nav -->
                <nav class="py-4">
                    <div id="logo">
                        <h1> <a href="trangchu"><span  aria-hidden="true"></span>Shoes-Giầy da hiện đại</a></h1>
                    </div>
                    <label for="drop" class="toggle">Menu</label>
                    <input type="checkbox" id="drop" />
                    <ul class="menu mt-2">
                        <li class="active"><a href="/minishop/trangchu">Trang chủ</a></li>
                       
                        <li><a href="/minishop/dashboard">dashboard</a></li>
                        <li>
                            <!-- First Tier Drop Down -->
                            <label for="drop-2" class="toggle">Danh mục sản phẩm<span class="fa fa-angle-down" aria-hidden="true"></span> </label>
                            <a href="#">Danh mục sản phẩm<span class="fa fa-angle-down" aria-hidden="true"></span></a>
                            <input type="checkbox" id="drop-2" />
                            <ul>
                                 <c:forEach var="valueDM" items="${ danhMuc}">
					              	  <li><a href="/minishop/sanpham/${valueDM.getMadanhmuc()}">${valueDM.getTendanhmuc() }</a></li>
					              </c:forEach>
                            </ul>
                        </li>
                     	<li><a href="/minishop/sodo">Sơ đồ trang</a></li>
                        <li><a href="/minishop/gopy">Góp ý</a></li>
                        <li>
                        <a href="/minishop/giohang" style="display: flex;color:white;">	
	                				<svg class="bi bi-archive" width="1.5em" height="1.5em" viewBox="0 0 20 20" fill="red" xmlns="http://www.w3.org/2000/svg">
									  <path fill-rule="evenodd" d="M4 7v7.5c0 .864.642 1.5 1.357 1.5h9.286c.715 0 1.357-.636 1.357-1.5V7h1v7.5c0 1.345-1.021 2.5-2.357 2.5H5.357C4.021 17 3 15.845 3 14.5V7h1z" clip-rule="evenodd"></path>
									  <path fill-rule="evenodd" d="M7.5 9.5A.5.5 0 018 9h4a.5.5 0 010 1H8a.5.5 0 01-.5-.5zM17 4H3v2h14V4zM3 3a1 1 0 00-1 1v2a1 1 0 001 1h14a1 1 0 001-1V4a1 1 0 00-1-1H3z" clip-rule="evenodd"></path>
									</svg>
									<div class="giohang">
										<span >${soLuongSanPham }sp</span>
									</div>
						</a>
                        </li>
                        <c:choose>
                        	<c:when test="${headerUser!=null }">
                        		 <li><a href="/minishop/signout" id="User">${headerUser} </a></li>
                        	</c:when>
                       <c:otherwise>
                    	 <li><a href="/minishop/login">Đăng nhập </a></li>
                       </c:otherwise>
                         </c:choose>
                    </ul>
                </nav>
                <!-- //nav -->
            </div>
        </header>
        <!-- //header -->
</body>
</html>