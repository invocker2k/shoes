<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
         <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Sơ đồ trang</title>
 		<jsp:include page="../headerallpages.jsp"></jsp:include>

</head>
<body>
<div class="main-banner inner" id="home" >
			<c:import url="header.jsp"></c:import>
</div>
	<div class="container">
		<div class="row">
		<div class="col-md-6 col-sm-12">
		<h3>DANH SÁCH SẢN PHẨM TRONG GIỎ HÀNG</h3>
			<table class="table">
				<thead>
					 <tr>
					  <th> Tên Sản phẩm</th>
				      <th scope="col">size</th>
				      <th scope="col">màu</th>
				      <th scope="col">Số lượng</th>
				        <th scope="col">Giá tiền</th>
					 </tr>
				</thead>
				<tbody>
						<c:forEach var="chitietsanpham" items="${giohangs }">
					      		<tr data-machitiet="${chitietsanpham.getMaChiTietSP() }">
				                			<td class="tenGH" data-masp="${ chitietsanpham.getMasp()}">${chitietsanpham.getTenSP() }</td>
				               		 		<td class="sizeGH" data-size="${chitietsanpham.getMasize()}">${chitietsanpham.getTensize()  }</td>
				               		 		<td class="mauGH" data-mau="${ chitietsanpham.getMamau()}">${chitietsanpham.getTenmau()}</td>
				               		 		<td class="soluongGH">
				               		 			<input type="number" class="soluong-giohang" min="1"  value="${chitietsanpham.getSoLuong()}"/> 
				               		 		</td>
				               		 		<td class="giatien" data-value="${chitietsanpham.getGiaTien()}">${chitietsanpham.getGiaTien()}</td>
				               		 		<td class="btn btn-danger xoaSP">
				               		 		 	Xóa
				               		 		</td>
				        			   </tr>
				             </c:forEach>
				</tbody>
			</table>
			<h4 >Tổng tiền: <span class="tinhtongtien"></span></h4>
		</div>
		<div class="col-md-6 col-sm-12">
			<h3>Thông tin người mua!</h3>
				<form method="post">
				<div class="form-group">
					<label for="tenkhachhang" >Tên người mua/Người nhận</label>
					<input id="tenkhachhang" name="tenkhachhang" class="form-control"/><br/>
					<label for="sdt" >Số điện thoại</label><br>
					<input id="sdt" name="sdt" class="form-control"/><br/>
					 <div class="radio">
					  <label><input type="radio" name="hinhthucgiaohang" value="Giao hàng tận nơi" checked>Giao hàng tận nơi</label>
					</div>
					<div class="radio">
					  <label><input type="radio" name="hinhthucgiaohang" value="Nhận hàng tại cửa hàng">Nhận hàng tại cửa hàng</label>
					</div><br/>
					<label for="noigiaohang" >Địa chỉ nhận hàng</label>
					<input id="noigiaohang" name="noigiaohang" class="form-control"/><br/>
					<label for="ghichu" >Ghi chú</label>
					<textarea id="ghichu" name="ghichu" class="form-control" rows="5"></textarea>
				</div>
						<input type="submit" class="btn-dathang btn btn-primary" value="Đặt hàng">
				
				</form>
			
		</div>
		
		</div>
		
		
	</div>	
<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>
