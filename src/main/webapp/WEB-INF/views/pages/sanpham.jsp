<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
         <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Danh mục sản phẩm</title>
 		<jsp:include page="../headerallpages.jsp"></jsp:include>

</head>
<body>
		<div class="main-banner inner" id="home" >
			<c:import url="header.jsp"></c:import>
		</div>
												  <!-- sản phẩm -->
    <section class="about py-5">
        <div class="container pb-lg-3">
            <h3 class="tittle text-center">${sigleDanhMuc.getTendanhmuc() }</h3>
            <div class="row">
            									<!-- for each -->
            <c:forEach var="sanpham" items="${sanPhamTheoDanhMuc}">
             <div class="col-md-4 product-men">
              <a href="singleshop/${sanpham.getMasanpham()}">
                    <div class="product-shoe-info shoe text-center">
                        <div class="men-thumb-item">
                            <img src="/minishop/resources/images/${sanpham.getHinhsanpham() }" class="img-fluid" alt="">
                            <span class="product-new-top">New</span>
                        </div>
                       
                        <div class="item-info-product">
                            <h4> <span>  ${sanpham.getTensanpham() } </span>  </h4>
                            <div class="product_price">
                                <div class="grid-price">
                                    <span class="money">${sanpham.getGiatien()  }.VND</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    </a>
                </div>
               
            </c:forEach>
                     
            <!-- xong phần sản phẩm -->

      		  </div>
     	   </div>
        </section>
<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>