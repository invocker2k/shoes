<%@ page language="java" contentType="text/html; UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
 		<jsp:include page="../headerallpages.jsp"></jsp:include>
  <link rel="stylesheet" href="/minishop/resources/css/sign.css">
 
   <script src="resources/js/sign.js"></script>
</head>

<body>
	<div style="font-size:15px;">
		<jsp:include page="header.jsp"></jsp:include>
	</div>
    <!-- Đăng Nhập -->
	<div class="cont" id="divdangnhap">
  <div class="demo">
    <div class="login divdangnhap"  >
      <div class="login__check"></div>
      <div class="login__form">
      <form action="" method="post">
              <span class="thatbai">${kiemtradangnhap}</span>
      	<a><span>${email}</span></a>
       <div class="login__row">
          <svg class="login__icon name svg-icon" viewBox="0 0 20 20">
            <path d="M0,20 a10,8 0 0,1 20,0z M10,0 a4,4 0 0,1 0,8 a4,4 0 0,1 0,-8" />
          </svg>
          <input name="Username" type="text" class="login__input name" placeholder="Username"/>
        </div>
        <div class="login__row">
          <svg class="login__icon pass svg-icon" viewBox="0 0 20 20">
            <path d="M0,20 20,20 20,8 0,8z M10,13 10,16z M4,8 a6,8 0 0,1 12,0" />
          </svg>
          <input type="password" name="password" class="login__input pass" placeholder="Password"/>
        </div>
        <button name="signin" type="submit" class="login__submit">Sign in</button>
        <p class="login__signup">Bạn chưa có tài khoản? &nbsp;<a onclick="onDangKi()">đăng kí!</a></p>
      </form>
       </div>
    </div>
      </div>
</div>
    <!-- het phan dang nhap -->
	<div class="cont" id="divdangki"  style="display: none">
  <div class="demo">
    <div class="login divdangki" >
      <div class="dangki__form">
      <form action="" method="post">
         <div class="login__row">
          <svg class="login__icon name svg-icon" viewBox="0 0 20 20">
            <path d="M0,20 a10,8 0 0,1 20,0z M10,0 a4,4 0 0,1 0,8 a4,4 0 0,1 0,-8" />
          </svg>
          <input name="hoten" type="text" class="login__input name" placeholder="Họ Tên"/>
        </div>
        <div class="login__row">
          <svg class="login__icon name svg-icon" viewBox="0 0 20 20">
            <path d="M0,20 a10,8 0 0,1 20,0z M10,0 a4,4 0 0,1 0,8 a4,4 0 0,1 0,-8" />
          </svg>
          <input name="diachi" type="text" class="login__input name" placeholder="Địa Chỉ"/>
        </div>
        <div class="login__row">
          <svg class="login__icon name svg-icon" viewBox="0 0 20 20">
            <path d="M0,20 a10,8 0 0,1 20,0z M10,0 a4,4 0 0,1 0,8 a4,4 0 0,1 0,-8" />
          </svg>
          <input name="gioitinh" type="text" class="login__input name" placeholder="Giới Tính"/>
        </div>
        <div class="login__row">
          <svg class="login__icon name svg-icon" viewBox="0 0 20 20">
            <path d="M0,20 a10,8 0 0,1 20,0z M10,0 a4,4 0 0,1 0,8 a4,4 0 0,1 0,-8" />
          </svg>
          <input name="cmnd" type="text" class="login__input name" placeholder="Số Cmnd"/>
        </div>
        <div class="login__row">
          <svg class="login__icon name svg-icon" viewBox="0 0 20 20">
            <path d="M0,20 a10,8 0 0,1 20,0z M10,0 a4,4 0 0,1 0,8 a4,4 0 0,1 0,-8" />
          </svg>
          <input name="email" type="text" class="login__input name" placeholder="Email"/>
        </div>
        <div class="login__row">
          <svg class="login__icon name svg-icon" viewBox="0 0 20 20">
            <path d="M0,20 a10,8 0 0,1 20,0z M10,0 a4,4 0 0,1 0,8 a4,4 0 0,1 0,-8" />
          </svg>
          <input name="machucvu" type="text" class="login__input name" placeholder="Chức VỤ"/>
        
        </div>
        <div class="login__row">
          <svg class="login__icon name svg-icon" viewBox="0 0 20 20">
            <path d="M0,20 a10,8 0 0,1 20,0z M10,0 a4,4 0 0,1 0,8 a4,4 0 0,1 0,-8" />
          </svg>
          <input name="tennhanvien" type="text" class="login__input name" placeholder="tennhanvien"/>
        </div>
        <div class="login__row">
          <svg class="login__icon pass svg-icon" viewBox="0 0 20 20">
            <path d="M0,20 20,20 20,8 0,8z M10,13 10,16z M4,8 a6,8 0 0,1 12,0" />
          </svg>
          <input name="matkhau" type="password" class="login__input__dk pass" placeholder="Mật khẩu"/>
           <input name="matkhau2" type="password" class="login__input__dk pass" placeholder="Nhập lại"/>
        </div>
          <p class="login__signup signinbtn">Bạn đã có tài khoản? &nbsp;<a onclick="onDangNhap()">đăng nhập</a></p>
     
        <button name="signup" type="submit" class="login__submit btnsignup">Đăng kí</button>
      	
      </form>
   
      </div>
    </div>
      </div>
</div>
	</body>
</html>