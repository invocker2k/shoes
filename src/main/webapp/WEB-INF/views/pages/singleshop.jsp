<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
         <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Sản phẩm</title>
    <!-- Custom-Files -->
    <link rel="stylesheet" href="../resources/css/bootstrap.css">
    <!-- Bootstrap-Core-CSS -->
    <link rel="stylesheet" href="../resources/css/style.css" type="text/css" media="all" />
    <!-- Style-CSS -->
    <!-- font-awesome-icons -->
    <link href="../resources/css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome-icons -->
    <!-- /Fonts -->
    <link href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">
    <!-- //Fonts -->
    <!-- js -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    
      <script src="../resources/js/custom.js"></script>
  </head>
<body>
		<div class="main-banner inner" id="home" style="box-shadow:5px 9px blue;">
			<c:import url="header.jsp"></c:import>
		</div>

				 <!---->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.html">Home</a>
        </li>
        <li class="breadcrumb-item active">Shop Single</li>
    </ol>
    <!---->
    <!-- banner -->
    <section class="ab-info-main py-md-5 py-4">
        <div class="container py-md-3">
            <!-- top Products -->
            <div class="row">
                <!-- product left -->
                <div class="side-bar col-lg-4">
		<!-- menu -->			
		<h2>Sơ đồ sản phẩm</h2>
           <ul class="menuSoDo">
              <c:forEach var="valueDM" items="${ danhMuc}">
              	  <li><a href="#">${valueDM.getTendanhmuc() }</a></li>
              </c:forEach>
               
           </ul>
		<!--Done menu -->
					           
                    <div class="deal-leftmk left-side">
                        <h3 class="sear-head">Giảm giá đặc biệt</h3>
                     <c:forEach var="sanpham" items="${listSP }">
                    	<a href="/minishop/singleshop/${sanpham.getMasanpham()}">
		                    	<div class="special-sec1 row mb-3">
		                            <div class="img-deals col-md-4">
		                                <img src="/minishop/resources/images/${sanpham.getHinhsanpham() }" class="img-fluid" alt="">
		                            </div>
		                            <div class="img-deal1 col-md-4">
		                                <h3>${sanpham.getTensanpham() }</h3>
		                                <span>${sanpham.getGiatien() } VND</span>
		                            </div>
		                       </div>
		                 </a>      
                    </c:forEach>
                    </div>
                    <!-- //deals -->

                </div>
                <!-- //product left -->
                <!-- product right -->
                <div class="left-ads-display col-lg-8">
                    <div class="row">
                        <div class="desc1-left col-md-6">
                            <img src="/minishop/resources/images/${sp.getHinhsanpham() }" class="img-fluid" alt="">
                        </div>
                        <div class="desc1-right col-md-6 pl-lg-4">
                            <h3 class="tenSP" data-maSP="${sp.getMasanpham() }">${sp.getTensanpham() }</h3>
                            <h5 style="color:red;" class="giaSP">${sp.getGiatien() } </h5> 
                            <div class="available mt-3">
                               	
                               	<table class="table table-dark">
                               	
                               	 <thead>
								    <tr>
								      <th scope="col">size</th>
								      <th scope="col">màu</th>
								      <th scope="col">Số lượng</th>
								    </tr>
								  </thead>
                               	<tbody>	
                               	
                                <c:forEach var="chitietsanpham" items="${sp.getChiTietSanPham() }">
                               		 	<tr>
                               		 		<td class="sizeSP" data-maSize=" ${chitietsanpham.getSizeSanPham().getMasize()  }">
                               		 		 ${chitietsanpham.getSizeSanPham().getSize()  }
                               		 		</td>
                               		 		<td class="mauSP" data-maMau="${chitietsanpham.getMauSanPham().getMamau()   }">
                               		 		 ${chitietsanpham.getMauSanPham().getTenmau()   }
                               		 		</td>
                               		 		<td class="soluongSP" ">
                               		 		 ${chitietsanpham.getSoluong()  }
                               		 		</td>
                               		 		<td>
                               		 		 <p><button class="btn btn-success btn-card" data-machitiet="${chitietsanpham.getMachitietsanpham()}">Giỏ hàng</button></p>
                               		 		</td>
		                      			 </tr>
		                        </c:forEach>
		                       
                               	</tbody>
                               	</table>
                               	<p>${sp.getMota()}</p>
		                        <button type="button" class="btn btn-outline-success btn-block">Đặt hàng</button>
                            </div>
                            <div class="share-desc">
                                <div class="share">
                                    <h4>Chia sẻ sản phẩm này :</h4>
                                    <ul class="w3layouts_social_list list-unstyled">
                                        <li>
                                            <a href="#" class="w3pvt_facebook">
                                                <span class="fa fa-facebook-f"></span>
                                            </a>
                                        </li>
                                        <li class="mx-2">
                                            <a href="#" class="w3pvt_twitter">
                                                <span class="fa fa-twitter"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="w3pvt_dribble">
                                                <span class="fa fa-dribbble"></span>
                                            </a>
                                        </li>
                                        <li class="ml-2">
                                            <a href="#" class="w3pvt_google">
                                                <span class="fa fa-google-plus"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row sub-para-w3layouts mt-5">
                        <h3 class="shop-sing">Lorem ipsum dolor sit amet</h3>
                  	  
                        <p class="mt-3 italic-blue">Consectetur adipisicing elPellentesque vehicula augue eget nisl ullamcorper, molestie blandit ipsum auctor. Mauris volutpat augue dolor.Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut lab ore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco. labore et dolore magna aliqua.</p>
                        <p class="mt-3">Lorem ipsum dolor sit amet, consectetur adipisicing elPellentesque vehicula augue eget nisl ullamcorper, molestie blandit ipsum auctor. Mauris volutpat augue dolor.Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut lab ore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco. labore et dolore magna aliqua.</p>
                    </div>
                    <h3 class="shop-sing">Featured Products</h3>
                    <div class="row m-0">
                        <div class="col-md-4 product-men">
                            <div class="product-shoe-info shoe text-center">
                                <div class="men-thumb-item">
                                    <img src="images/s10.jpg" class="img-fluid" alt="">
                                    <span class="product-new-top">New</span>
                                </div>
                                <div class="item-info-product">
                                    <h4>
                                        <a href="shop-single.html">Suitable Lace Up </a>
                                    </h4>
                                    <div class="product_price">
                                        <div class="grid-price">
                                            <span class="money">$675.00</span>
                                        </div>
                                    </div>
                                    <ul class="stars">
                                        <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                                        <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                                        <li><a href="#"><span class="fa fa-star-half-o" aria-hidden="true"></span></a></li>
                                        <li><a href="#"><span class="fa fa-star-half-o" aria-hidden="true"></span></a></li>
                                        <li><a href="#"><span class="fa fa-star-o" aria-hidden="true"></span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 product-men">
                            <div class="product-shoe-info shoe text-center">
                                <div class="men-thumb-item">
                                    <img src="images/s11.jpg" class="img-fluid" alt="">
                                    <span class="product-new-top">New</span>
                                </div>
                                <div class="item-info-product">
                                    <h4>
                                        <a href="shop-single.html">Black Flats</a>
                                    </h4>

                                    <div class="product_price">
                                        <div class="grid-price">
                                            <span class="money">$475.00</span>
                                        </div>
                                    </div>
                                    <ul class="stars">
                                        <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                                        <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                                        <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                                        <li><a href="#"><span class="fa fa-star-half-o" aria-hidden="true"></span></a></li>
                                        <li><a href="#"><span class="fa fa-star-o" aria-hidden="true"></span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 product-men">
                            <div class="product-shoe-info shoe text-center">
                                <div class="men-thumb-item">
                                    <img src="images/s12.jpg" class="img-fluid" alt="">
                                    <span class="product-new-top">New</span>
                                </div>
                                <div class="item-info-product">
                                    <h4>
                                        <a href="shop-single.html">Elevator Shoes </a>
                                    </h4>

                                    <div class="product_price">
                                        <div class="grid-price">
                                            <span class="money">$575.00</span>
                                        </div>
                                    </div>
                                    <ul class="stars">
                                        <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                                        <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                                        <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                                        <li><a href="#"><span class="fa fa-star-half-o" aria-hidden="true"></span></a></li>
                                        <li><a href="#"><span class="fa fa-star-o" aria-hidden="true"></span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- //contact -->
			
				<jsp:include page="footer.jsp"></jsp:include>
			
</body>
</html>